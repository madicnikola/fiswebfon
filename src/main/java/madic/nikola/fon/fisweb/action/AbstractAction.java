/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package madic.nikola.fon.fisweb.action;

import javax.servlet.http.HttpServletRequest;
import madic.nikola.fon.fisweb.action.impl.LoginAction;

/**
 *
 * @author Nikola @FON
 */
public abstract class AbstractAction {

    public abstract String execute(HttpServletRequest request);

    public boolean checkSession(HttpServletRequest request) {

        return this.getClass().equals(LoginAction.class) ? true : request.getSession(false).getAttribute("loginUser") != null;
    }
}
